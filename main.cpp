#include <stack>

int matchPar(std::string data){
	static char map[256] = { 0 }; 
	std::stack<char> stack;
	map['{'] = 1;
	map['['] = 1;
	map['('] = 1;
	map['}'] = '{';
	map[']'] = '[';
	map[')'] = '(';

	int dataLength = data.length();
	for (int i = 0; i < dataLength; ++i) {
		char currChar = data[i];
		char op = map[currChar];
		if (op == 0) {
		}
		else if (op == 1) {
			stack.push(currChar);
		}
		else {
			if (stack.empty()) {
				return -1;
			}
			else {
				char open = stack.top();
				stack.pop();
				if (open != op) {
					return -1;
				}
			}
		}
	}

	if (!stack.empty()) {
		return -1;
	}

	return 0;
}

int main() {
    return 0;
}